#include <Eigen/StdVector>
#include <iostream>

typedef Eigen::Matrix<double, 2, 1> Vec2;
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Vec2)

typedef Eigen::Matrix<double, 3, 4> Mat34;
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Mat34)

struct Ray
{
    Ray(const Mat34& P_, const Vec2& p_)
        : P(P_)
        , p(p_)
    {
    }
    Mat34 P;
    Vec2 p;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Ray)

int main(int argc, char* argv[])
{
#ifdef __AVX__
    std::cout << "AVX enabled" << std::endl;
#else
    std::cout << "AVX disabled" << std::endl;
#endif

    std::vector<Ray> rays;

    for (int i = 0; i<100; i++)
    {
        auto P = Mat34::Random();
        auto p = Vec2::Random();

        // Both statements fail when AVX is enabled. Either with an unaligned array assert
        // or an access violation in Eigen::internal::pstore<double,__m256d>(double * to, const __m256d & from) 
        rays.emplace_back(P, p);
        rays.push_back(Ray{ P, p });
    }
}
